<?php

use Bitrix\Main;

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler("sale", "OnSaleOrderBeforeSaved", ["OrderEvent", "onOrderSave"]);
$eventManager->addEventHandler("iblock", "OnBeforeIBlockSectionUpdate", ["DropSectionUpdate", "OnBeforeIBlockSectionUpdateHandler"]);
$eventManager->addEventHandler('catalog', 'OnSuccessCatalogImport1C', 'OnSuccessCatalogImport1CHandler');
/*
use MP\Autoloader\CAutoloader;

require_once __DIR__ . '/lib/MP/Autoloader/IAutoloader.php';
require_once __DIR__ . '/lib/MP/Autoloader/AAutoloader.php';
require_once __DIR__ . '/lib/MP/Autoloader/CAutoloader.php';

require_once __DIR__ . '/lib/Mobile_Detect.php';

$oAutoloader = new CAutoloader();
$oAutoloader->addPrefix('\MP', __DIR__ . '/lib/MP');
$oAutoloader->register();
*/

// События для заказа

class OrderEvent
{
    public function onOrderSave(Event $event)
    {
        // получим объект заказа
        $order = $event->getParameter("ENTITY");

        $propertyCollection = $order->getPropertyCollection();

        /** @var \Bitrix\Sale\PropertyValue $obProp */
        foreach ($propertyCollection as $obProp) {
            $arProp = $obProp->getProperty();

            if (isset($_REQUEST['utm_source'])) {
                if (!in_array($arProp["CODE"], ["UTM_SOURCE"])) {
                    continue;
                }

                // установим значение свойства и запищем куки
                setcookie("UTM_SOURCE", $_REQUEST['utm_source']);
                $obProp->setValue($_REQUEST['utm_source']);
            }
        }
    }
}

// Работа с полями элементов при импорте

class DropSectionUpdate
{
    function OnBeforeIBlockSectionUpdateHandler(&$arFields)
    {
        if (@$_REQUEST['mode'] === 'import') {
            unset($arFields['NAME']);
            unset($arFields['CODE']);
        }
    }
}

function OnSuccessCatalogImport1CHandler()
{
    $mess = 'Импорт завершен успешно';

    $rsUser = CUser::GetByID(1);
    $arUser = $rsUser->Fetch();

    $arEventFields = [
        "MESSAGE" => $mess,
        "EMAIL_TO" => $arUser['EMAIL'],
    ];

    CEvent::Send("1C_SUCCESS", SITE_ID, $arEventFields);
}

function printer($prVar)
{
    echo '<pre>';
    print_r($prVar);
    echo '</pre>';
}
